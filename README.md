# OpenML dataset: PS4-Games

https://www.openml.org/d/43352

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset include all games for PlayStation 4 for the present.
I used the truetrophies website to create this dataset.
Content
You can find 1 datasets :
games_data.csv: contend up to date list of PlayStation 4 (PS4) games , games name and some details like score ,rating for each game etc.
This dataset includes 1584 games information  
Acknowledgements
The data in this dataset has been scraped using BeautifulSoup from the truetrophies website
Inspiration
what you can do with this data is :
1- Popularity based recommenders system 
you can recommend games by taking the count of number of ratings given to each place.
The assumption is, the game that has the most number of ratings are the popular. 
2-content based recommenders system 
 you can recommend games based on its features and how similar they are to features of other games in the data set.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43352) of an [OpenML dataset](https://www.openml.org/d/43352). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43352/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43352/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43352/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

